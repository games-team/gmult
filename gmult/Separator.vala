/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

public class Separator : Gtk.DrawingArea
{
  private void draw_request (Gtk.DrawingArea _this, Cairo.Context ctx, int w, int h)
  {
    var style = get_style_context ();
    style.save();

    var color = style.get_color();
    ctx.set_source_rgba(color.red, color.green, color.blue, color.alpha);

    ctx.move_to(0, 0);
    ctx.line_to(w, h);
    ctx.stroke();

    style.restore();
  }

  public override void measure(Gtk.Orientation orientation, int for_size,
                               out int minimum, out int natural,
                               out int minimum_baseline,
                               out int natural_baseline)
  {
    if (orientation == Gtk.Orientation.HORIZONTAL) {
      minimum = -1;
      natural = -1;
    } else {
      minimum = 1;
      natural = 1;
    }
    minimum_baseline = -1;
    natural_baseline = -1;
  }

  construct
  {
    hexpand = true;
    set_draw_func(draw_request);
  }
}

