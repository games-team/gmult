/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

// Prints a character as big as its allocation allows
public class ScalingLetter : Gtk.Widget
{
  public string letter {get; set;}

  public ScalingLetter(string? letter)
  {
    Object(letter: letter);
  }

  ///////

  public override void snapshot(Gtk.Snapshot snap)
  {
    var width = get_width();
    var height = get_height();
    var layout = create_pango_layout(letter);

    var minlen = int.min(width, height);
    var points = (int) (minlen * (72.0/dpi));

    var attrs = new Pango.AttrList();
    attrs.insert(Pango.attr_weight_new(Pango.Weight.BOLD));
    attrs.insert(new Pango.AttrSize.with_absolute(points * Pango.SCALE));
    layout.set_attributes(attrs);

    Pango.Rectangle logical;
    layout.get_pixel_extents(null, out logical);
    var x = 0.5 * (width - logical.width) - logical.x;
    var y = 0.5 * (height - logical.height) - logical.y;

    var context = get_style_context();
    snap.render_layout(context, x, y, layout);
  }

  int dpi;

  construct
  {
    var settings = Gtk.Settings.get_default();
    dpi = settings.gtk_xft_dpi / Pango.SCALE;

    hexpand = true;
    vexpand = true;

    notify["letter"].connect(queue_draw);
  }
}

