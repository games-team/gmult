/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

public class GMultApp : Adw.Application
{
  WeakRef main_window;

  const OptionEntry[] options = {
    {"version", 0, 0, OptionArg.NONE, null, N_("Show version"), null},
    {null}
  };

  const ActionEntry[] actions = {
    {"new-game", new_game},
    {"hint", show_hint},
    {"about", about},
    {"quit", quit},
  };

  public GMultApp()
  {
    Object(application_id: "net.launchpad.gmult");
    add_main_option_entries(options);
  }

  public override int handle_local_options(VariantDict options)
  {
    if (options.contains("version")) {
      print("%s\n", Config.VERSION);
      return 0;
    }
    return -1;
  }

  // Eventually, when we can assume that the system supports color schemes,
  // we can drop this legacy check.
  bool has_dark_gtk_theme()
  {
    // libadwaita will call this for us, but we need it now to check the
    // settings - it's safe to call this multiple times.
    Gtk.init();

    var theme_name = Gtk.Settings.get_default().gtk_theme_name.casefold();
    var dark_suffix = "-dark".casefold();
    return theme_name.has_suffix(dark_suffix); // very rough heuristic
  }

  public override void startup()
  {
    // grab this before libadwaita overrides it
    var dark_gtk_theme = has_dark_gtk_theme();

    base.startup();

    add_action_entries(actions, this);
    set_accels_for_action("app.new-game", {"<Primary>n"});
    set_accels_for_action("app.hint", {"<Primary>h"});
    set_accels_for_action("app.quit", {"<Primary>q", "<Primary>w"});

    var display = Gdk.Display.get_default();
    var style_manager = Adw.StyleManager.get_for_display(display);

    if (!style_manager.system_supports_color_schemes && dark_gtk_theme) {
      // We can't follow the gtk theme as it changes, but this is good
      // enough for now - start up with the right dark/light preference.
      style_manager.color_scheme = Adw.ColorScheme.PREFER_DARK;
    }

    if (Environment.get_variable("GMULT_DEMO") == "1")
    {
      // Use default GNOME settings as much as possible.
      // The goal here is that we are suitable for screenshots.
      var gtksettings = Gtk.Settings.get_for_display(display);

      gtksettings.gtk_decoration_layout = ":close";
      gtksettings.gtk_font_name = "Cantarell 11";
      gtksettings.gtk_icon_theme_name = "Adwaita";
      style_manager.color_scheme = Adw.ColorScheme.FORCE_LIGHT;
    }
  }

  public override void activate()
  {
    base.activate();

    if (get_app_window() == null) {
      main_window.set(new GtkMult(this));
    }
    get_app_window().present();
  }

  GtkMult? get_app_window()
  {
    return main_window.get() as GtkMult;
  }

  void new_game()
  {
    get_app_window().new_puzzle();
  }

  void show_hint()
  {
    get_app_window().show_hint();
  }

  void about()
  {
    var dialog = new Adw.AboutWindow();
    dialog.application_icon = "net.launchpad.gmult";
    dialog.application_name = Environment.get_application_name();
    dialog.artists = {
      "Jakub Steiner",
    };
    dialog.developers = {
      "Michael Terry",
    };
    dialog.issue_url = "https://bugs.launchpad.net/gmult/+filebug";
    dialog.license_type = Gtk.License.GPL_3_0;
    dialog.release_notes = """
      <ul>
        <li>New icon!</li>
        <li>New About dialog (this dialog!)</li>
        <li>Remember window size between runs</li>
      </ul>
    """;
    dialog.transient_for = get_app_window();
    dialog.translator_credits = _("translator-credits");
    dialog.version = Config.VERSION;
    dialog.website = "https://launchpad.net/gmult";
    dialog.present();
  }
}

int main(string[] args)
{
  Intl.textdomain(Config.GETTEXT_PACKAGE);
  Intl.bindtextdomain(Config.GETTEXT_PACKAGE, Config.LOCALE_DIR);
  Intl.bind_textdomain_codeset(Config.GETTEXT_PACKAGE, "UTF-8");

  Environment.set_prgname("net.launchpad.gmult");
  Environment.set_application_name(_("Multiplication Puzzle"));

  Gtk.Window.set_default_icon_name("net.launchpad.gmult");

  return new GMultApp().run(args);
}
