/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

using GLib;

public class CharBox : Adw.Bin
{
  public string letter { get; construct; default = "?"; }
  public signal void clicked();

  private Gtk.StateFlags _highlight = Gtk.StateFlags.NORMAL;
  public Gtk.StateFlags highlight {
    get {return _highlight;}
    set {
      _highlight = value;
      button.set_state_flags(value, true);
    }
  }
  
  /* Constructor */
  public CharBox (string letter)
  {
    Object(letter: letter);
  }
  
  Gtk.Button button;
  construct
  {
    var css = """
#charbox:hover {
  color: @accent_fg_color;
  background-color: @accent_bg_color;
}

#charbox:active {
  color: @accent_fg_color;
  background-color: @accent_bg_color;
}
    """;
    var provider = new Gtk.CssProvider();
    provider.load_from_data(css.data);

    var scaling_letter = new ScalingLetter(null);
    bind_property("letter", scaling_letter, "letter", BindingFlags.SYNC_CREATE);

    button = new Gtk.Button();
    button.add_css_class("circular");
    button.add_css_class("opaque");
    button.add_css_class("text-button");
    button.name = "charbox";
    button.child = scaling_letter;
    button.clicked.connect(() => {clicked();});
    button.get_style_context().add_provider(provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION);

    child = button;
  }
}

