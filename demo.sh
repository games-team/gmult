#!/bin/sh
# -*- Mode: sh; indent-tabs-mode: nil; tab-width: 2 -*-
#
# SPDX-License-Identifier: GPL-3.0-or-later
# SPDX-FileCopyrightText: Michael Terry

# This script sets up gmult to quickly make screenshots.

set -e

export GMULT_DEMO=1

rm -f ~/.var/app/net.launchpad.gmult/config/glib-2.0/settings/keyfile
make run
