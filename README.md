<!--
SPDX-License-Identifier: CC-BY-SA-4.0
SPDX-FileCopyrightText: Michael Terry
-->

# Multiplication Puzzle

Multiplication Puzzle is a simple game inspired by the
multiplication game inside the popular editor emacs.

You are presented with a long multiplication problem
where a 3-digit number is multiplied by a 2-digit number,
yielding two intermediate 4-digit number and a final
5-digit answer.  However, all the digits are replaced
by letters.

Your job is to discover which letters are which digits.

## To Play

You can make guesses in one of three ways:
1) Drag a digit to the letter you think it is.
2) Click on a letter or digit, then on your guess.
3) Type a letter or digit, then your guess.

If you are right, all instances of that letter will be replaced
by the digit.

