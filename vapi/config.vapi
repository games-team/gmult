/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-
 *
 * SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: Michael Terry
 */

[CCode (cprefix = "", lower_case_cprefix = "")]
namespace Config {
  public const string GETTEXT_PACKAGE;
  public const string LOCALE_DIR;
  public const string VERSION;
}

